"use strict";

function addTable() {
    // thead
    var thNode = document.createElement('th');
    thNode.innerHTML = '#';
    var headRow = document.createElement('tr');
    headRow.appendChild(thNode);
    var theadNode = document.createElement('thead');
    theadNode.appendChild(headRow);

    // tbody
    var num = document.getElementById('inputNumber').value;
    var tbodyNode = document.createElement('tbody');
    for (var i = 0; i < num; i++) {
        var tdNode = document.createElement('td');
        tdNode.innerHTML = 1 + i;
        var trNode = document.createElement('tr');
        trNode.appendChild(tdNode);
        tbodyNode.appendChild(trNode);
    }

    // table
    var tableNode = document.createElement('table')
    tableNode.setAttribute('class', 'table');
    tableNode.appendChild(theadNode);
    tableNode.appendChild(tbodyNode);

    var placrholder = document.getElementById('placeholder');
    placrholder.appendChild(tableNode);
}