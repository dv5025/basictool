"use strict";

function addForm() {
    var node1 = document.createElement('p')
    node1.innerHTML = 'Please fill in this form to create account'
    node1.setAttribute('class', 'lead')

    var input1 = document.createElement('input')
    input1.setAttribute('type', 'text')
    input1.setAttribute('class', 'form-control')
    input1.setAttribute('id', 'inputFirstName')
    input1.setAttribute('placeholder', 'First name')

    var input2 = document.createElement('input')
    input2.setAttribute('type', 'text')
    input2.setAttribute('class', 'form-control')
    input2.setAttribute('id', 'inputLastName')
    input2.setAttribute('placeholder', 'Last name')

    var input3 = document.createElement('input')
    input3.setAttribute('type', 'text')
    input3.setAttribute('class', 'form-control')
    input3.setAttribute('id', 'inputEmail')
    input3.setAttribute('placeholder', 'Email')

    var divInput1 = document.createElement('div')
    divInput1.setAttribute('class', 'form-group col-6')
    divInput1.appendChild(input1)

    var divInput2 = document.createElement('div')
    divInput2.setAttribute('class', 'form-group col-6')
    divInput2.appendChild(input2)

    var divInput3 = document.createElement('div')
    divInput3.setAttribute('class', 'form-group col-12')
    divInput3.appendChild(input3)

    var divRow = document.createElement('div')
    divRow.setAttribute('class', 'row')
    divRow.appendChild(divInput1)
    divRow.appendChild(divInput2)
    divRow.appendChild(divInput3)

    var formNode = document.createElement('form')
    formNode.appendChild(divRow)

    var placeholder = document.getElementById('placeholder')
    placeholder.appendChild(node1)
    placeholder.appendChild(formNode)
}