"use strict";

function addElement() {
    var node = document.createElement('p')
    node.innerHTML = 'Hello world'
    var placeHolder = document.getElementById('placeholder')
    placeHolder.appendChild(node)
}

function addHref() {
    var node = document.createElement('a')
    node.innerHTML = 'google'
    node.setAttribute('href', 'https://www.google.com')
    node.setAttribute('target', '_blank')
    var placeHolder = document.getElementById('placeholder')
    placeHolder.appendChild(node)
}

function addBoth() {
    var node = document.createElement('p')
    node.innerHTML = 'Hello World'
    var node2 = document.createElement('a')
    node2.innerHTML = 'google'
    node2.setAttribute('href', 'https://www.google.com')
    node2.setAttribute('target', '_blank')
    var placeHolder = document.getElementById('placeholder')
    placeHolder.appendChild(node)
    placeHolder.appendChild(node2)
}