"use strict";

function showSummary() {
    var emptyAll = true
    var inputBox = document.getElementsByName('inputText')
    var summaryElement = document.getElementById('summary')
    for (var i = 0; i < inputBox.length; i++) {
        if (inputBox[i].value != "") {
            emptyAll = false
            break
        }
    }

    if (!emptyAll) {
        var firstName = document.getElementById('inputFirstName').value
        var lastName = document.getElementById('inputLastName').value
        var email = document.getElementById('inputEmail').value
        var password = document.getElementById('inputPassword').value
        var confirmPass = document.getElementById('inputConfirmPassword').value


        summaryElement.innerHTML = "First name: " + firstName
        summaryElement.innerHTML = summaryElement.innerHTML + "<br>Last name: " + lastName
        summaryElement.innerHTML = summaryElement.innerHTML + "<br>Email: " + email
        summaryElement.innerHTML = summaryElement.innerHTML + "<br>Password: " + password
        summaryElement.innerHTML = summaryElement.innerHTML + "<br>Confirm password: " + confirmPass
        summaryElement.removeAttribute('hidden')
    } else {
        summaryElement.setAttribute('hidden', 'true')
    }
}