"use strict";

function listFormElement() {
    var formElements = document.getElementsByTagName('form');
    console.log(formElements)
}

function copyTextFromNameToLastName() {
    var nameElement = document.getElementById('inputFirstName')
    var surnameElement = document.getElementById('inputLastName')
    var firstname = nameElement.value
    console.log('firstname is ' + firstname)
    console.log('now lastname is ' + surnameElement.value)
    surnameElement.value = firstname
}

function showSummary() {
    var name = document.getElementById('inputFirstName').value
    var summaryElement = document.getElementById('summary')

    summaryElement.innerHTML = name
    summaryElement.removeAttribute('hidden')
}