"use strict";

function showSummary() {
    var firstName = document.getElementById('inputFirstName').value
    var lastName = document.getElementById('inputLastName').value
    var email = document.getElementById('inputEmail').value
    var password = document.getElementById('inputPassword').value
    var confirmPass = document.getElementById('inputConfirmPassword').value
    var stuStatus = document.querySelector('input[name="stuStatus"]:checked').value
    var summaryElement = document.getElementById('summary')

    summaryElement.innerHTML = "First name: " + firstName
    summaryElement.innerHTML = summaryElement.innerHTML + "<br>Last name: " + lastName
    summaryElement.innerHTML = summaryElement.innerHTML + "<br>Email: " + email
    summaryElement.innerHTML = summaryElement.innerHTML + "<br>Password: " + password
    summaryElement.innerHTML = summaryElement.innerHTML + "<br>Confirm password: " + confirmPass
    summaryElement.innerHTML = summaryElement.innerHTML + "<br>Student status: " + stuStatus
    summaryElement.removeAttribute('hidden')
}