"use strict";

function addTable() {
    // thead
    var headerText = ['#', 'First', 'Last', 'Handle'];
    var theadNode = document.createElement('thead');
    var headRow = document.createElement('tr');
    for (var i = 0; i < headerText.length; i++) {
        var node = document.createElement('th');
        node.innerHTML = headerText[i];
        node.setAttribute('scpoe', 'col');
        headRow.appendChild(node);
    }
    theadNode.appendChild(headRow);

    //tbody
    var data = [
        ['1', 'Mark', 'Otto', '@mdo'],
        ['2', 'Jacob', 'Thornton', '@fat'],
        ['3', 'Larry', 'the Bird', '@twitter']
    ];
    var tbodyNode = document.createElement('tbody')
    for (var i = 0; i < data.length; i++) {
        var bodyRow = document.createElement('tr');
        var thNode = document.createElement('th');
        thNode.innerHTML = data[i][0];
        thNode.setAttribute('scpoe', 'row');
        bodyRow.appendChild(thNode);

        for (var j = 1; j < data[i].length; j++) {
            var tdNode = document.createElement('td');
            tdNode.innerHTML = data[i][j];
            bodyRow.appendChild(tdNode);
        }

        tbodyNode.appendChild(bodyRow);
    }

    var tableNode = document.createElement('table');
    tableNode.setAttribute('class', 'table table-striped');
    tableNode.appendChild(theadNode);
    tableNode.appendChild(tbodyNode);

    var placeholder = document.getElementById('placeholder');
    placeholder.appendChild(tableNode);
}